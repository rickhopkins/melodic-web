<?php
/**
 * @name Home
 * Handles all requests for the Home controller
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Controllers
{
	/** list other namespaces */
	use Melodic\MVC\MvcController;
	
	class Home extends MvcController
	{
		/**
		 * Show the home page
		 */
		public function Index()
		{
			$this->view();
		}
	}
}
?>