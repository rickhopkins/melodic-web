<?php
/** get the required file */
require_once "melodic/core.php";

/** create RouteMap and execute */
$route = new Melodic\MVC\Route();
$route->render();
?>